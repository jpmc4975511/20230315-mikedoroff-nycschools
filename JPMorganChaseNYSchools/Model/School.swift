//
//  School.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/15/23.
//

import Foundation

struct School: Codable, Identifiable, Hashable {

    let dbn: String
    let id = UUID().uuidString
    let schoolName: String
    let boro: String
    let overviewParagraph: String
    let academicopportunities1: String?
    let academicopportunities2: String?
    let academicopportunities3: String?
    let academicopportunities4: String?
    let academicopportunities5: String?
    let languageClasses: String?
    let advancedplacementCourses: String?
    let neighborhood: String
    let campusName: String?
    let location: String
    let phoneNumber: String?
    let schoolEmail: String?
    let website: String
    let subway: String
    let bus: String
    let totalStudents: String
    let startTime: String?
    let endTime: String?
    let extracurricularActivities: String?
    let psalSportsBoys: String?
    let psalSportsGirls: String?
    let psalSportsCoed: String?
    let graduationRate: String?
    var collegeCareerRate: String?
    let latitude: String?
    let longitude: String?
    let borough: String?
    
    var unwrappedCollegeCareerRate: Double {
        if let collegeCareerRate = collegeCareerRate {
            return Double(collegeCareerRate) ?? 0.00
        } else {
            return 0.00
        }
    }
    
    var unwrappedAndConvertedLatitude: Double {
        if let latitude = latitude {
            return Double(latitude) ?? 0.00
        } else {
            return 0.00
        }
    }
    
    var unwrappedAndConvertedLongitude: Double {
        if let longitude = longitude {
            return Double(longitude) ?? 0.00
        } else {
            return 0.00
        }
    }

    enum CodingKeys: String, CodingKey {
        case dbn
        case id
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"
        case academicopportunities1
        case academicopportunities2
        case academicopportunities3
        case academicopportunities4
        case academicopportunities5
        case languageClasses = "language_classes"
        case advancedplacementCourses = "advancedplacement_courses"
        case neighborhood
        case campusName
        case location
        case phoneNumber
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case totalStudents = "total_students"
        case startTime = "start_time"
        case endTime = "end_time"
        case extracurricularActivities = "extracurricular_activities"
        case psalSportsBoys = "psal_sports_boys"
        case psalSportsGirls = "psal_sports_girls"
        case psalSportsCoed = "psal_sports_coed"
        case graduationRate = "graduation_rate"
        case collegeCareerRate = "college_career_rate"
        case latitude
        case longitude
        case borough
    }
}
