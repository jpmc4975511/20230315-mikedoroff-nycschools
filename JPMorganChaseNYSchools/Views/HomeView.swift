//
//  ContentView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import SwiftUI

struct HomeView: View {
    
    @StateObject var nySchoolsViewModel: NYSchoolsViewModel
    @State var errorAlert = false
    @State var errorMessage = ""
    @State var search = ""
    
    var body: some View {
        NavigationStack {
            VStack {
                Text("NYC Schools By Borough")
                    .font(.system(size: 24))
                // You must show a ProgressView to the user when there is an asynchronous task occuring.
                if nySchoolsViewModel.isUpdating {
                    ProgressView()
                } else {
                    CollegeAcceptanceView()
                    List {
                        ForEach(nySchoolsViewModel.schools, id: \.id) { school in
                            SchoolView(school: school, nySchoolsViewModel: nySchoolsViewModel)
                        }
                        .listStyle(.plain)
                    }
                }
            }
        }
        .searchable(text: $search)
        .onSubmit(of: .search) {
            nySchoolsViewModel.searchForSchoolByName(searchString: $search.wrappedValue)
        }
        .alert(isPresented: $errorAlert) {
            Alert(title: Text("Network Error"), message: Text("\n\(errorMessage) \n\nPlease check your network, restart the application and try again."), dismissButton: .default(Text("Ok")))
                }
        .task {
            do {
                try await nySchoolsViewModel.fetchSchoolDataFromEndpoint(schoolUrl: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!, satUrl: URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!)
            } catch  {
                print(error)
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(nySchoolsViewModel: NYSchoolsViewModel())
    }
}
