//
//  JPMorganChaseNYSchoolsTests.swift
//  JPMorganChaseNYSchoolsTests
//
//  Created by Doroff, Mike on 3/14/23.
//

import XCTest
@testable import JPMorganChaseNYSchools
final class JPMorganChaseNYSchoolsTests: XCTestCase {
    
    var schoolNetwork: Network<School>!
    var satNetwork: Network<SchoolSAT>!

    override func setUpWithError() throws {
        
        schoolNetwork = Network<School>()
        satNetwork = Network<SchoolSAT>()
        
    }

    override func tearDownWithError() throws {
        schoolNetwork = nil
        satNetwork = nil
    }

    func testSchoolNetwork() throws {
        Task {
            await schoolNetwork.fetchData(url: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!) { result in
                switch result {
                case .success(let schools):
                    XCTAssertTrue(!schools.isEmpty)
                case .failure(let err):
                    print("Failed", err)
                }
            }
        }
    }
    
    func testSatNetwork() throws {
        Task {
            await satNetwork.fetchData(url: URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!) { result in
                switch result {
                case .success(let schools):
                    XCTAssertTrue(!schools.isEmpty)
                case .failure(let err):
                    print("Failed", err)
                }
            }
        }
    }
}
